FROM nginx:alpine
RUN rm -rf /etc/nginx/conf.d/*
RUN echo 'server { \
    listen 80; \
    listen [::]:80; \
    server_name localhost; \
    root /usr/share/nginx/html/; \
    location / { \
        try_files $uri $uri/ $uri.html =404; \
    } \
}' > /etc/nginx/conf.d/default.conf
COPY ./itscool/build /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]